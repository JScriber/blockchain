import threading
from queue import Queue

from blockchain.blockchain import Blockchain


def miner(name: str, blockchain: Blockchain, queue: Queue):
    print(f'Miner {name} started!')

    transactions = queue.get()
    print(f'{len(transactions)} transactions received by {name}')

    queue.task_done()
    blockchain.generate_block(transactions)


def main():
    chain = Blockchain()
    queue = Queue(maxsize=0)
    miners = 2

    jobs = [threading.Thread(target=miner, daemon=True, args=(str(m), chain, queue)) for m in range(0, miners)]

    for j in jobs:
        j.start()

    queue.put(["salut", "mon"])
    queue.put(["salut", "mon"])

    for j in jobs:
        j.join()

    print(chain)
    print("Valid blockchain" if chain.is_valid_chain() else "Invalid blockchain")


if __name__ == '__main__':
    main()
