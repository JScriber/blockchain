from typing import List

from blockchain.encryption.hash import hash_content


def merge_hash(list_hash: List[str]) -> List[str]:
    grouped = [list_hash[i: i + 2] for i in range(0, len(list_hash), 2)]
    return [hash_content("".join(group)) for group in grouped]


def merkle_tree(transactions: List[str]) -> str:
    list_hashes = [hash_content(i) for i in transactions]

    while len(list_hashes) > 1:
        list_hashes = merge_hash(list_hashes)

    return list_hashes[0]
