from typing import List
from blockchain.block import Block


class Blockchain:
    blocks: List[Block]

    def __init__(self):
        self.blocks = []
        self.create_genesis_block()

    def create_genesis_block(self):
        """
        Creates the first block of the blockchain.
        """
        self.blocks.append(
            Block(
                previous_hash=None,
                transactions=[]
            )
        )

    def generate_block(self, transactions: List[str]):
        """
        Add a block to the blockchain.
        :param transactions:
        """
        last_block = self.get_latest_block()
        block = Block(
            previous_hash=last_block.hash,
            transactions=transactions
        )

        # 10 min elapsed...

        if self.is_valid_new_block(block):
            print("Congrats! You mined one block.")
            self.blocks.append(block)
        else:
            print("Ouch! Someone changed the last block!")

    def is_valid_new_block(self, block: Block) -> bool:
        """
        Check if someone hasn't changed the last block in the meantime.
        :param block:
        :return: TRUE or FALSE
        """
        return self.get_latest_block().hash == block.previous_hash

    def is_valid_chain(self) -> bool:
        """
        Says if the blockchain is valid.
        Checks each block, and the genesis block.
        """
        for i, block in enumerate(self.blocks[::-1]):
            current_index = len(self.blocks) - 1 - i

            if current_index == 0:
                valid_genesis = self.is_valid_genesis_block(block)
                print("Is valid genesis block" if valid_genesis else "Invalid genesis")
                return valid_genesis
            else:
                previous_block_index = current_index - 1

                if previous_block_index >= 0:
                    previous_block = self.blocks[previous_block_index]

                    if previous_block.hash != block.previous_hash:
                        return False

        return True

    def get_latest_block(self):
        return self.blocks[-1]

    def is_valid_genesis_block(self, block: Block) -> bool:
        """
        Check if the given block is a valid genesis block.
        :param block:
        :return: TRUE or FALSE
        """
        return block.previous_hash is None and len(block.transactions) == 0

    def __str__(self):
        output = ""
        for block in self.blocks:
            output += "- {}\n".format(str(block))

        return f'Blocks ({len(self.blocks)}):\n{output}'
