from datetime import datetime
from typing import List, Optional
from random import randint

from blockchain.encryption.hash import hash_content
from blockchain.encryption.merkle import merkle_tree


class Block:
    previous_hash: Optional[str]
    hash: str
    transactions: List[str]
    timestamp: float
    nonce: int

    def __init__(self, previous_hash, transactions):
        self.nonce = 0
        self.previous_hash = previous_hash
        self.transactions = transactions
        self.timestamp = datetime.timestamp(datetime.now())
        self.compute_hash()

    def compute_hash(self):
        merkle_root = merkle_tree(self.transactions) if len(self.transactions) > 0 else None
        computed_hash: Optional[str] = None
        complexity = randint(1, 5)

        while computed_hash is None or not computed_hash.startswith("0" * complexity):
            self.nonce += 1
            computed_hash = hash_content(self.__get_payload(merkle_root))

        self.hash = computed_hash

    def __get_payload(self, merkle_root: Optional[str]) -> str:
        payload = f'{self.previous_hash} {self.timestamp} {self.nonce}'

        if merkle_root is not None:
            payload += f' {merkle_root}'

        return payload

    def __repr__(self):
        return f'Hash: {self.hash} | ' \
               f'Prev: {self.previous_hash} | ' \
               f'Time: {self.timestamp} | ' \
               f'Transactions: {len(self.transactions)} | ' \
               f'Nonce: {self.nonce}'
